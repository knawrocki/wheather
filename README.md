# wheather


## Requirements
* Docker version 18.06 or later
* Docker compose version 1.22 or later
* An editor or IDE

Note: OS recommendation - Linux Ubuntu based.

## Components
1. Nginx 1.23
2. PHP 8.2 fpm
3. PostgreSQL 15
4. Symfony 6

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Setting up DEV environment

### 1. Clone repository

```
git clone https://gitlab.com/knawrocki/wheather.git
```

### 2. Set another APP_SECRET for application in .env file

Note 1: You can get unique secret key for example [here](http://nux.net/secret).

Note 2: Do not use .env.local.php on dev and test environment (delete it if exist).


### 3. Add domain to local 'hosts' file (/etc/hosts):

```bash
127.0.0.1    localhost
```

### 4. Configure `/docker/dev/xdebug.ini` (Linux/Windows) (optional):

- In case you need debug only requests with IDE KEY: PHPSTORM from frontend in your browser:
```bash
xdebug.start_with_request = no
```
Install locally in Firefox extension "Xdebug helper" and set in settings IDE KEY: PHPSTORM

- In case you need debug any request to an api (by default):
```bash
xdebug.start_with_request = yes
```

Note: For prod environment another password should be used.

### 5.Build, start and install the docker images from your terminal:
```bash
make build
make start
make composer-install
make generate-jwt-keys
```

### 6. Make sure that you have updated database schema and installed migrations:
```bash
make db-schema-update
make migrate
```

### 7. Create/remove OAuth2 client:

OAuth2 client data (identifier and secret) will be generated and displayed by using command:

```bash
make create-oauth2-client
```

OAuth2 client data can be removed by using command:

```bash
make delete-oauth2-client
```

### 8. Install tools (code quality etc.):
```bash
make tools-install
```

### 9. Execute tool commands (if you need):

Open bash console:

```bash
make ssh-supervisord
```

In console execute tools commands from points 9.1 - 9.8, if you need check application code quality, errors detecting, code refactoring etc.

#### 9.1. Execute command for unit tests
```bash
make phpunit
```

#### 9.2. Execute command for static code analysis
```bash
make phpstan
```

#### 9.3. Execute command for Easy Coding Standard tool (with command to fix issues)
```bash
make ecs
```
```bash
make ecs-fix
```

#### 9.4. Updates code coverage on coveralls.io. Note: COVERALLS_REPO_TOKEN should be set on CI side.
```bash
make report-code-coverage 
```

#### 9.5. Runs Php Insights analysis tool
```bash
make phpinsights 
```

#### 9.6. Runs php mess detector
```bash
make phpmd
```

#### 9.7. Generates phpmetrics static analysis report
```bash
make phpmetrics
```

#### 9.8. Runs Instant Upgrade and Automated Refactoring of any PHP code (option)
```bash
make rector
```

### 10. Final touch:

#### 10.1. Open browser and use example url (check web action):
http://localhost:8000/pogoda/wroclaw/full (full data)

![Path mappings](docs/images/04_web_page.png)

http://localhost:8000/pogoda/wroclaw (only temperature data) 

![Path mappings](docs/images/05_web_page_temperature.png)

#### 10.2. Open API client (like Postman) use example url (check api action):

Configure OAuth2 authorization with client data from point 7:
![Path mappings](docs/images/01_postman_oauth2.png)

Authorization:

![Path mappings](docs/images/02_authorization.png)

Get new access token and use them:

![Path mappings](docs/images/03_use_token.png)

Configure request uri and send request:
http://localhost:8000/api/pogoda/wroclaw/full (full data)

![Path mappings](docs/images/06_api_request_full.png)

http://localhost:8000/api/pogoda/wroclaw (only temperature data) 

![Path mappings](docs/images/07_api_request_temperature.png)

## Setting up PROD environment locally
1.You can clone this repository from GitHub or install via composer.

```
git clone https://gitlab.com/knawrocki/wheather.git
```

2.Edit docker-compose-prod.yml and set necessary user/password for PostgreSQL etc.

3.Edit env.prod and set necessary user/password for PostgreSQL etc.

4.Build, start and install the docker images from your terminal:
```bash
make build-prod
make start-prod
make generate-jwt-keys
```

5.Make sure that you have installed migrations / created roles and groups / created oauth2 client:
```bash
make migrate-no-test
make create-roles-groups
make create-oauth2-client
```

## List of available commands:

```bash
make build                       Build dev environment
make build-prod                  Build prod environment
make build-test                  Build test or continuous integration environment
make composer-install            Installs composer dependencies
make composer-install-no-dev     Installs composer no-dev dependencies
make composer-normalize          Normalizes composer.json file content
make composer-require-checker    Checks the defined dependencies against your code
make composer-unused             Shows unused packages by scanning and comparing package namespaces against your code
make composer-update             Updates composer dependencies
make composer-validate           Validates composer.json file content
make db-schema-update            Runs schema update for main/test databases
make drop-migrate                Drops databases and runs all migrations for the main/test databases
make ecs                         Runs Easy Coding Standard tool
make ecs-fix                     Runs Easy Coding Standard tool to fix issues
make env-prod                    Creates cached config file .env.local.php (usually for prod environment)
make generate-jwt-keys           Generates RSA keys for JWT
make help                        Shows available commands with description
make info                        Shows Php and Symfony version
make logs-nginx                  Shows logs from the nginx container. Use ctrl+c in order to exit
make logs-pgsql                  Shows logs from the pgsql container. Use ctrl+c in order to exit
make migrate                     Runs all migrations for main/test databases
make migrate-no-test             Runs all migrations for main database
make phpcpd                      Runs php copy/paste detector
make phpcs                       Runs PHP CodeSniffer (unstable)
make phpinsights                 Runs Php Insights analysis tool
make phpmd                       Runs php mess detector
make phpmetrics                  Generates phpmetrics static analysis report
make phpstan                     Runs PhpStan static analysis tool
make phpunit                     Runs PhpUnit tests
make rector                      Runs Instant Upgrade and Automated Refactoring of any PHP code
make report-code-coverage        Updates code coverage on coveralls.io. Note: COVERALLS_REPO_TOKEN should be set on CI side.
make restart                     Stop and start dev environment
make restart-prod                Stop and start prod environment
make restart-test                Stop and start test or continuous integration environment
make ssh-nginx                   Get bash inside nginx docker container
make ssh-pgsql                   Get bash inside pgsql docker container
make ssh-supervisord             Get bash inside supervisord docker container (cron jobs running there, etc...)
make start                       Start dev environment
make start-prod                  Start prod environment
make start-test                  Start test or continuous integration environment
make stop                        Stop dev environment
make stop-prod                   Stop prod environment
make stop-test                   Stop test or continuous integration environment

```


## License
[The MIT License (MIT)](LICENSE)
