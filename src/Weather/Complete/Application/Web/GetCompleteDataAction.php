<?php

declare(strict_types=1);

namespace App\Weather\Complete\Application\Web;

use App\Shared\Application\Web\WebAction;
use App\Weather\General\Infrastructure\Service\DataResolverService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetCompleteDataAction extends WebAction
{
    public const ROUTE_NAME = 'web.get.weather.complete';

    public function __construct(private readonly DataResolverService $dataResolverService)
    {
    }

    #[Route('/pogoda/{miasto}/full', name: self::ROUTE_NAME, methods: ['GET'])]
    public function __invoke(string $miasto): Response
    {
        $data = $this->dataResolverService->getData($miasto);

        return $this->render('ui/weather/full.html.twig', [
            'data' => $data,
        ]);
    }
}

