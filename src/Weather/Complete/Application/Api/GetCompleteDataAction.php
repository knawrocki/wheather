<?php

declare(strict_types=1);

namespace App\Weather\Complete\Application\Api;

use App\Shared\Application\Api\ApiAction;
use App\Weather\Complete\Infrastructure\Normalizer\Api\ResponseDataNormalizer;
use App\Weather\General\Infrastructure\Service\DataResolverService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class GetCompleteDataAction extends ApiAction
{
    public const ROUTE_NAME = 'api.get.weather.complete';

    public function __construct(
        private readonly DataResolverService $dataResolverService,
        private ResponseDataNormalizer $normalizer
    ) {
    }

    #[Route('/pogoda/{miasto}/full', name: self::ROUTE_NAME, methods: ['GET'])]
    #[IsGranted("ROLE_OAUTH2_GETFULLDATA")]
    public function __invoke(string $miasto): Response
    {
        $data = $this->dataResolverService->getData($miasto);
        $dataNormalized = $this->normalizer->normalize($data);

        return $this->json($dataNormalized);
    }
}

