<?php

declare(strict_types=1);

namespace App\Weather\Complete\Infrastructure\Normalizer\Api;

use App\Weather\General\Domain\ResponseDataInterface;

final class ResponseDataNormalizer
{
    public const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';

    public function normalize(ResponseDataInterface $responseData): array
    {
        return [
            'city_id'              => $responseData->getCityId(),
            'city'                 => $responseData->getCity(),
            'measurement_datetime' => $responseData->getMeasurementDateTime()?->format(self::DEFAULT_DATE_FORMAT),
            'temperature'          => $responseData->getTemperature(),
            'wind_speed'           => $responseData->getWindSpeed(),
            'wind_direction'       => $responseData->getWindDirection(),
            'relative_humidity'    => $responseData->getRelativeHumidity(),
            'total_rainfall'       => $responseData->getTotalRainfall(),
            'pressure'             => $responseData->getPressure(),
        ];
    }
}
