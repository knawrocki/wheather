<?php

declare(strict_types=1);

namespace App\Weather\General\Infrastructure\Service;

use App\Weather\General\Domain\ResponseDataInterface;
use App\Weather\General\Infrastructure\Provider\DataProviderInterface;

class DataResolverService
{

    public function __construct(public DataProviderInterface $provider)
    {
    }

    public function getData(string $citySlug): ?ResponseDataInterface
    {
        return $this->provider->getData($citySlug);
    }
}
