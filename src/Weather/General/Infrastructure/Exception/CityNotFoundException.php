<?php

declare(strict_types=1);

namespace App\Weather\General\Infrastructure\Exception;

use App\Shared\Infrastructure\Exception\ApiException;
use App\Shared\Infrastructure\Exception\WebException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CityNotFoundException extends BadRequestHttpException implements ApiException, WebException
{
    private const MESSAGE = 'Invalid city name';

    public function __construct(string $message = self::MESSAGE, int $code = Response::HTTP_BAD_REQUEST)
    {
        parent::__construct(message: $message, code: $code);
    }

    public function getParams(): ?array
    {
        return [];
    }
}
