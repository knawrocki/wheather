<?php

declare(strict_types=1);

namespace App\Weather\General\Infrastructure\Exception;

use App\Shared\Infrastructure\Exception\ApiException;
use App\Shared\Infrastructure\Exception\WebException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

class ApiUnavailableException extends ServiceUnavailableHttpException implements ApiException, WebException
{
    private const MESSAGE = 'Weather API is temporary unavailable';

    public function __construct(
        string $message = self::MESSAGE,
    ) {
        parent::__construct(message: $message);
    }

    public function getParams(): ?array
    {
        return [];
    }
}
