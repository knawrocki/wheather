<?php

declare(strict_types=1);

namespace App\Weather\General\Infrastructure\Provider\Json;

use App\Weather\General\Domain\IMGWResponseData;
use App\Weather\General\Infrastructure\Exception\ApiUnavailableException;
use App\Weather\General\Infrastructure\Exception\CityNotFoundException;
use App\Weather\General\Infrastructure\Provider\DataProviderInterface;
use ErrorException;
use Symfony\Component\Serializer\SerializerInterface;

final class IMGWDataProvider implements DataProviderInterface
{
    private const FORMAT = 'json';
    private const STATION_DATA_PATH = 'data/synop/station';
    private const TEST_PATH = 'data/synop/id/12500';

    private string $url;

    public function __construct(public string $apiUrl, public SerializerInterface $serializer)
    {
        $testUrl = sprintf("%s/%s", $apiUrl, self::TEST_PATH);
        $this->checkApi($testUrl);

        $this->url = sprintf("%s/%s", $apiUrl, self::STATION_DATA_PATH);
    }

    public function getData(string $city): ?IMGWResponseData
    {
        $data = $this->getJsonData($city);

        if (!isset($data)) {
            return null;
        }

        return $this->serializer->deserialize($data, IMGWResponseData::class, self::FORMAT);
    }

    private function getJsonData(string $city): ?string
    {
        try {
            $data = file_get_contents(sprintf("%s/%s", $this->url, strtolower($city)));
        } catch (ErrorException $exception) {
            throw new CityNotFoundException();
        }

        if ($data === false) {
            return null;
        }

        return $data;
    }

    private function checkApi(string $testUrl): void
    {
        try {
            file_get_contents($testUrl);
        } catch (ErrorException $exception) {
            throw new ApiUnavailableException();
        }
    }
}
