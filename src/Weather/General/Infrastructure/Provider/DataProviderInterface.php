<?php

declare(strict_types=1);

namespace App\Weather\General\Infrastructure\Provider;

use App\Weather\General\Domain\ResponseDataInterface;

interface DataProviderInterface
{
    public function getData(string $city): ?ResponseDataInterface;
}
