<?php

declare(strict_types=1);

namespace App\Weather\General\Domain;

use DateTime;
use Symfony\Component\Serializer\Annotation\SerializedName;

class IMGWResponseData implements ResponseDataInterface
{

    /**
     * @SerializedName("id_stacji")
     */
    public ?string $cityId = null;

    /**
     * @SerializedName("stacja")
     */
    public ?string $city = null;

    /**
     * @SerializedName("data_pomiaru")
     */
    public ?string $measurementDate = null;

    /**
     * @SerializedName("godzina_pomiaru")
     */
    public ?string $measurementHour = null;

    /**
     * @SerializedName("temperatura")
     */
    public ?string $temperature = null;

    /**
     * @SerializedName("predkosc_wiatru")
     */
    public ?string $windSpeed = null;

    /**
     * @SerializedName("kierunek_wiatru")
     */
    public ?string $windDirection = null;

    /**
     * @SerializedName("wilgotnosc_wzgledna")
     */
    public ?string $relativeHumidity = null;

    /**
     * @SerializedName("suma_opadu")
     */
    public ?string $totalRainfall = null;

    /**
     * @SerializedName("cisnienie")
     */
    public ?string $pressure = null;

    public function getCityId(): ?string
    {
        return $this->cityId;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getMeasurementDate(): ?string
    {
        return $this->measurementDate;
    }

    public function getMeasurementHour(): ?string
    {
        return $this->measurementHour;
    }

    public function getTemperature(): ?string
    {
        return $this->temperature;
    }

    public function getWindSpeed(): ?string
    {
        return $this->windSpeed;
    }

    public function getWindDirection(): ?string
    {
        return $this->windDirection;
    }

    public function getRelativeHumidity(): ?string
    {
        return $this->relativeHumidity;
    }

    public function getTotalRainfall(): ?string
    {
        return $this->totalRainfall;
    }

    public function getPressure(): ?string
    {
        return $this->pressure;
    }

    /**
     * @throws \Exception
     */
    public function getMeasurementDateTime(): ?DateTime
    {
        if (empty($date = $this->getMeasurementDate()) || empty($time = $this->getMeasurementHour())) {
            return null;
        }

        $timeString = (strlen($time) === 1 ? '0' . $time : (strlen($time) === 2 ? $time . ':00:00' : $time));
        $dateTimeString = sprintf("%s %s", $date, $timeString);

        return new DateTime($dateTimeString);
    }
}
