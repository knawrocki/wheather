<?php

declare(strict_types=1);

namespace App\Weather\General\Domain;

use DateTimeInterface;

interface ResponseDataInterface
{
    public function getCityId(): ?string;

    public function getCity(): ?string;

    public function getMeasurementDate(): ?string;

    public function getMeasurementHour(): ?string;

    public function getMeasurementDateTime(): ?DateTimeInterface;

    public function getTemperature(): ?string;

    public function getWindSpeed(): ?string;

    public function getWindDirection(): ?string;

    public function getRelativeHumidity(): ?string;

    public function getTotalRainfall(): ?string;

    public function getPressure(): ?string;
}
