<?php

declare(strict_types=1);

namespace App\Weather\Temperature\Infrastructure\Normalizer\Api;

use App\Weather\General\Domain\ResponseDataInterface;

final class ResponseDataNormalizer
{
    public const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';

    public function normalize(ResponseDataInterface $responseData): array
    {
        return [
            'city'                 => $responseData->getCity(),
            'measurement_datetime' => $responseData->getMeasurementDateTime()?->format(self::DEFAULT_DATE_FORMAT),
            'temperature'          => $responseData->getTemperature(),
        ];
    }
}
