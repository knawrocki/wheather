<?php

declare(strict_types=1);

namespace App\Weather\Temperature\Application\Web;

use App\Shared\Application\Web\WebAction;
use App\Weather\General\Infrastructure\Service\DataResolverService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetTemperatureDataAction extends WebAction
{
    public const ROUTE_NAME = 'web.get.weather.temperature';

    public function __construct(private readonly DataResolverService $dataResolverService)
    {
    }

    #[Route('/pogoda/{miasto}', name: self::ROUTE_NAME, methods: ['GET'])]
    public function __invoke(string $miasto): Response
    {
        $data = $this->dataResolverService->getData($miasto);

        return $this->render('ui/weather/temperature.html.twig', [
            'data' => $data,
        ]);
    }
}

