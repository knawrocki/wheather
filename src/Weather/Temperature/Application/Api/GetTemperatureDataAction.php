<?php

declare(strict_types=1);

namespace App\Weather\Temperature\Application\Api;

use App\Shared\Application\Api\ApiAction;
use App\Weather\General\Infrastructure\Service\DataResolverService;
use App\Weather\Temperature\Infrastructure\Normalizer\Api\ResponseDataNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class GetTemperatureDataAction extends ApiAction
{
    public const ROUTE_NAME = 'api.get.weather.temperature';

    public function __construct(
        private readonly DataResolverService $dataResolverService,
        private ResponseDataNormalizer $normalizer
    ) {
    }

    #[Route('/pogoda/{miasto}', name: self::ROUTE_NAME, methods: ['GET'])]
    #[IsGranted("ROLE_OAUTH2_GETTEMPERATURE")]
    public function __invoke(string $miasto): Response
    {
        $data = $this->dataResolverService->getData($miasto);
        $dataNormalized = $this->normalizer->normalize($data);

        return $this->json($dataNormalized);
    }
}

