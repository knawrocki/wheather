<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Security\Api;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class OAuth2Provider implements UserProviderInterface
{

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        return $user;
    }

    /**
     * @inheritDoc
     */
    public function supportsClass(string $class): bool
    {
        return $class === ApiUserKey::class;
    }

    /**
     * @inheritDoc
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        return new ApiUserKey(ApiUserKey::DEFAULT_API_ROLE, $identifier);
    }
}
