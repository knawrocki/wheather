<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Security\Api;

use Symfony\Component\Security\Core\User\UserInterface;

class ApiUserKey implements UserInterface
{

    private const DEFAULT_API_USERNAME = 'REST_API';
    public const DEFAULT_API_ROLE = 'ROLE_API';

    private ?string $userRole;
    private ?string $userName;

    public function __construct(?string $userRole = null, ?string $userName = null)
    {
        $this->userName = $userName;
        $this->userRole = $userRole;

        if (!$userName) {
            $this->userName = self::DEFAULT_API_USERNAME;
        }

        if (!$userRole) {
            $this->userRole = self::DEFAULT_API_ROLE;
        }
    }

    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        return [$this->userRole];
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials(): void
    {
        unset($this->userName, $this->userRole);
    }

    /**
     * @inheritDoc
     */
    public function getUserIdentifier(): string
    {
        return $this->userName;
    }

    public function __toString(): string
    {
        return $this->userName;
    }
}
