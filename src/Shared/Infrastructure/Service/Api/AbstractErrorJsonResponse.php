<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Service\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractErrorJsonResponse
{
    protected const DEFAULT_TRANSLATE_DOMAIN = 'api';

    public function __construct(protected TranslatorInterface $translatorInterface)
    {
    }

    abstract public function prepare(
        Request $request,
        ?ErrorMessage $message,
        int $errorCode
    ): JsonResponse;

    protected function getErrorCode(int $errorCode): string
    {
        return strtoupper(str_replace(' ', '_', Response::$statusTexts[$errorCode]));
    }

    public function getErrorMessage(?ErrorMessage $message): ?string
    {
        return $message ? $this->translatorInterface->trans(
            $message->getBody(),
            $message->getParams(),
            self::DEFAULT_TRANSLATE_DOMAIN
        ) : null;
    }
}
