<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Service\Api;

final readonly class ErrorMessage
{
    public function __construct(private string $body = '', private array $params = [])
    {
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getParams(): array
    {
        return $this->params;
    }
}
