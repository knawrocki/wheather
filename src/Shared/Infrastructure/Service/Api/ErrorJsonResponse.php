<?php

namespace App\Shared\Infrastructure\Service\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ErrorJsonResponse extends AbstractErrorJsonResponse
{

    public function prepare(Request $request, ?ErrorMessage $message, int $errorCode): JsonResponse
    {
        $error = [
            'code'    => $this->getErrorCode($errorCode),
            'message' => $this->getErrorMessage($message),
        ];

        return new JsonResponse(
            [
                'errors' => [
                    $error
                ],
                'request-id' => $request->getSession()->get('request-id'),
            ],
            $errorCode
        );
    }
}
