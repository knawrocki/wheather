<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventListener;

use App\Shared\Infrastructure\Service\Api\ErrorJsonResponse;
use App\Shared\Infrastructure\Service\Api\ErrorMessage;
use DateTime;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Contracts\Translation\TranslatorInterface;

class ResponseListener extends AbstractHttpListener
{

    public function __construct(
        private readonly string $logDir,
        private readonly ErrorJsonResponse $errorJsonResponse,
        TranslatorInterface $translatorInterface
    ) {
        parent::__construct($translatorInterface);
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        if ($this->createLog($request, $response) === false) {
            return;
        }

        if ($response->getStatusCode() === Response::HTTP_TOO_MANY_REQUESTS) {
            $response->headers->set(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE_VALUE);
            $this->createLog($request, $response, Level::Error);
        }

        if ($response->getStatusCode() === Response::HTTP_METHOD_NOT_ALLOWED) {
            $response->headers->set(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE_VALUE);

            $event->setResponse(
                $this->errorJsonResponse->prepare(
                    $request,
                    new ErrorMessage('Invalid API request method'),
                    Response::HTTP_METHOD_NOT_ALLOWED
                )
            );

            $this->createLog($request, $response, Level::Error);

            $event->stopPropagation();
        }

        if ($response->getStatusCode() === Response::HTTP_BAD_REQUEST) {
            $response->headers->set(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE_VALUE);
            $this->setNormalizedApiExceptions($request, $response);

            $this->createLog($request, $response, Level::Error);
        }

        if ($response->getStatusCode() === Response::HTTP_NOT_FOUND) {
            $responseData = json_decode($response->getContent(), true);

            if (!isset($responseData['errors'])) {
                $response->setContent(null);
                $event->setResponse($response);
                $event->stopPropagation();
            }
            $this->createLog($request, $response, Level::Error);
        }

        if ($response->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) {
            $event->setResponse(
                $this->errorJsonResponse->prepare(
                    $request,
                    new ErrorMessage('Server error'),
                    Response::HTTP_INTERNAL_SERVER_ERROR
                )
            );

            $this->createLog($request, $response, Level::Critical);

            $event->stopPropagation();
        }
    }

    private function createLog(Request $request, Response $response, Level $level = Level::Info): bool
    {
        $loggerName = static::isApiRequest($request) ? 'API' : 'WEB';
        $fileName = (new DateTime())->format('Y-m-d');
        $path = 'response' . DIRECTORY_SEPARATOR . strtolower($loggerName);

        $logger = new Logger($loggerName);

        $logger->pushHandler(new StreamHandler("{$this->logDir}/{$path}/{$fileName}.log", $level));
        $logger->info($fileName, [
            'method'   => $request->getMethod(),
            'url'      => $request->getUri(),
            'ip'       => $request->getClientIp(),
            'request'  => $request->getContent(),
            'headers'  => $this->prepareHeaders($request->headers->all()),
            'response' => $response->getContent(),
        ]);

        return true;
    }
}
