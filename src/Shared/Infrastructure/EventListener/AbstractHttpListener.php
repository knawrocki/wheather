<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventListener;

use App\Shared\Infrastructure\Exception\ApiException;
use App\Shared\Infrastructure\Service\Api\ErrorJsonResponse;
use App\Shared\Infrastructure\Service\Api\ErrorMessage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractHttpListener
{
    protected const API_PREFIX = '/api';
    protected const CONTENT_TYPE_HEADER = 'Content-type';
    protected const CONTENT_TYPE_VALUE = 'application/json';

    protected const HEADERS_TO_LOG = [
        'content-type',
        'accept',
    ];

    protected function __construct(protected TranslatorInterface $translatorInterface)
    {
    }

    public static function isApiRequest(Request $request): bool
    {
        return str_starts_with($request->getPathInfo(), self::API_PREFIX);
    }

    protected function prepareHeaders(array $headers): array
    {
        $result = [];
        foreach ($headers as $header => $content) {
            if (in_array(strtolower($header), self::HEADERS_TO_LOG, true)) {
                $result[strtolower($header)] = $content;
            }
        }

        return $result;
    }

    protected function setNormalizedApiExceptions(Request $request, Response $response): void
    {
        $exception = $request->attributes->get('exception');

        if (!$exception) {
            return;
        }

        if ($exception instanceof ApiException) {
            $jsonException = $this->setErrorResponse($request, $exception);
            $response->setContent($jsonException->getContent());
            $response->setContentSafe();
        }
    }

    protected function setErrorResponse(
        Request $request,
        ApiException $exception
    ): JsonResponse {
        $errorMessage = new ErrorMessage($exception->getMessage(), $exception->getParams());

        return (new ErrorJsonResponse($this->translatorInterface))->prepare(
            $request,
            $errorMessage,
            $exception->getCode()
        );
    }
}
