<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventListener;

use DateTime;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestListener extends AbstractHttpListener
{

    public function __construct(private readonly string $logDir)
    {
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $request = $event->getRequest();

        $this->createLog($request);
    }

    private function createLog(Request $request): void
    {
        $loggerName = static::isApiRequest($request) ? 'API' : 'WEB';
        $fileName = (new DateTime())->format('Y-m-d');
        $path = 'request' . DIRECTORY_SEPARATOR . strtolower($loggerName);

        $logger = new Logger($loggerName);

        $logger->pushHandler(new StreamHandler("{$this->logDir}/{$path}/{$fileName}.log", Level::Info));
        $logger->info($fileName, [
            'method'  => $request->getMethod(),
            'url'     => $request->getUri(),
            'ip'      => $request->getClientIp(),
            'request' => $request->getContent(),
            'headers' => $this->prepareHeaders($request->headers->all()),
        ]);
    }
}
