<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Exception;

interface ApiException
{
    public function getCode();
    public function getMessage(): string;
    public function getParams(): ?array;
}
